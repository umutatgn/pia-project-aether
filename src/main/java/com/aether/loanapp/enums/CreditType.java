package com.aether.loanapp.enums;

public enum CreditType {
    Mortgage,
    Student,
    Personal,
    Small_Business,
    Land,
    Other
}
