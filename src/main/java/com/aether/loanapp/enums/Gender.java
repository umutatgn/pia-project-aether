package com.aether.loanapp.enums;

public enum Gender {
    MRS,
    MR,
    OTHER
}
