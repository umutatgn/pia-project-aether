package com.aether.loanapp.model;

import com.aether.loanapp.enums.CreditType;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import lombok.Data;

import javax.persistence.*;
import javax.persistence.criteria.CriteriaBuilder;
import java.io.Serializable;

@Entity
@Data
@Table
@JsonIgnoreProperties(value = {"hibernateLazyInitializer","handler"})
public class Credit implements Serializable {
    @Id
    @SequenceGenerator(sequenceName = "creditIdGenerator",name = "creditIdGenerator",allocationSize = 1,initialValue = 1)
    @GeneratedValue(generator = "creditIdGenerator",strategy = GenerationType.AUTO)
    private Long creditid;
    @Column(nullable = false)
    private Long amount;
    private Double monthlyinstallment;
    @Column(nullable = false)
    private Integer installment;
    private Integer activeinstallment;
    private CreditType creditType;

    @ManyToOne(fetch = FetchType.LAZY,cascade = CascadeType.ALL)
    @JoinColumn(name = "customer_id",referencedColumnName = "tcid")
    private Customer customer;

    public Credit() {
    }

    public Credit(Long creditid, Long amount, Integer installment,CreditType creditType) {
        this.amount = amount;
        this.installment = installment;
        this.creditType = creditType;
        this.activeinstallment = 0;
        this.monthlyinstallment = Double.valueOf(amount/installment);
    }

    public Credit(Long amount, Integer installment, Customer customer,CreditType creditType) {
        this.amount = amount;
        this.installment = installment;
        this.creditType = creditType;
        this.customer = customer;
        this.monthlyinstallment = Double.valueOf(amount/installment);
    }

    public Credit(Long amount, Integer installment, Integer activeinstallment) {
        this.amount = amount;
        this.installment = installment;
        this.activeinstallment = activeinstallment;
        this.monthlyinstallment = Double.valueOf(amount/installment);
    }
}
