package com.aether.loanapp.service;

import com.aether.loanapp.dto.CreditDto;
import com.aether.loanapp.model.Credit;
import com.aether.loanapp.repository.CreditRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class CreditService implements ICredit {
    @Autowired
    CreditRepository creditRepository;

    public List<Credit> getCredits(){
        return creditRepository.findAll();
    }

    public Credit addCredit(Credit c1){
        return creditRepository.save(c1);
    }

    public List<Credit> getCreditsByTcId(Long tcid){
        return creditRepository.findAllByTcId(tcid);
    }

    @Override
    public Credit creditDtoToCredit(CreditDto creditDto) {
        Credit c1 = new Credit();
        c1.setAmount(creditDto.getAmount());
        c1.setInstallment(creditDto.getInstallment());
        c1.setActiveinstallment(creditDto.getActiveinstallment());
        c1.setMonthlyinstallment(Double.valueOf(creditDto.getAmount()/creditDto.getInstallment()));
        return c1;
    }

    @Override
    public CreditDto creditToCreditDto(Credit credit) {
        return null;
    }

}
