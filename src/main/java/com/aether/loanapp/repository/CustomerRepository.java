package com.aether.loanapp.repository;

import com.aether.loanapp.model.Customer;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import java.util.Optional;

@Repository
public interface CustomerRepository extends JpaRepository<Customer,Long> {
    //@Query(value = "select * from customer where tcid = :tcid",nativeQuery = true)
    Customer getById(Long tcid);
}
